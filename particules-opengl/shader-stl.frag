#version 450

uniform int light_number;
uniform vec4[10] light_positions;
uniform vec4[10] light_colors;
uniform float[10] light_intensities;
uniform vec4 light_ambient;

in vec3 normal;
in vec4 cColor;
in vec4 positionCS;

out vec4 color;

void main()
{

	vec4 lightColor = vec4(0,0,0,0);

	for(int i = 0; i < light_number; ++i)
	{
		float distance = length(light_positions[i] - positionCS);
		vec4 light = normalize(light_positions[i] - positionCS);
		vec4 cam = normalize(-positionCS);
		float cos_theta = max(dot(vec4(normal, 1), light), 0);
		float cos_theta_p = max(dot(light, cam), 0);

		lightColor += light_colors[i] * light_intensities[i] *(0.625f * cos_theta + 0.375f * +  pow(cos_theta_p, 100)) / pow(distance, 2);
		//lightColor += light_colors[i] * (0.625f * cos_theta + 0.375f * +  pow(cos_theta_p, 100));
	}

	color = cColor * (0.2f * light_ambient + 0.8f * lightColor);
}

