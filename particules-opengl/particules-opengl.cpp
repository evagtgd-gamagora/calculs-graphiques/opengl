// particules-opengl.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#define TINYPLY_IMPLEMENTATION
#define GLM_ENABLE_EXPERIMENTAL
#include <tinyply.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/projection.hpp>
#include "stl.h"
#include "texture.h"

#include <vector>
#include <iostream>
#include <random>
#include <sstream>
#include <fstream>
#include <string>

static void error_callback(int /*error*/, const char* description)
{
	std::cerr << "Error: " << description << std::endl;
}

void APIENTRY opengl_error_callback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar *message,
	const void *userParam)
{
	std::cout << message << std::endl;
}

static void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mods*/)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}

/* PARTICULES */
struct Particule {
	glm::vec3 position;
	glm::vec3 color;
	glm::vec3 speed;
};

void MoveParticule(Particule& particule)
{
	particule.position.y -= 0.01f;

	if (particule.position.y < -1)
	{
		particule.position.y += 2;
	}
}

void MoveParticules(std::vector<Particule>& particules)
{
	for (std::vector<Particule>::iterator p = particules.begin(); p != particules.end(); ++p)
	{
		MoveParticule(*p);
	}
}

std::vector<Particule> MakeParticules(const int n)
{
	std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution01(0, 1);
	std::uniform_real_distribution<float> distributionWorld(-1, 1);

	std::vector<Particule> p;
	p.reserve(n);

	for (int i = 0; i < n; i++)
	{
		float color = distribution01(generator);
		p.push_back(Particule{
					   {
						distributionWorld(generator),
						distributionWorld(generator),
						distributionWorld(generator)
					   },
					   {
						color, color, 1.0f
					   },
					   {0.f, 0.f, 0.f}
			});
	}

	return p;
}

/*MIRROR*/
struct Mirror
{
	glm::vec3 pos1;
	glm::vec3 pos2;
};

struct MirrorVertex
{
	glm::vec3 pos;
	glm::vec2 uv;
};

std::vector<MirrorVertex> MakeMirrorVertex(Mirror m)
{
	std::vector<MirrorVertex> v;
	v.reserve(6);

	MirrorVertex mv1;
	mv1.pos = m.pos1;
	mv1.uv = glm::vec2(1, 1);

	MirrorVertex mv2;
	mv2.pos = glm::vec3(m.pos1.x, m.pos2.y, m.pos1.z);
	mv2.uv = glm::vec2(1, 0);

	MirrorVertex mv3;
	mv3.pos = m.pos2;
	mv3.uv = glm::vec2(0, 0);

	MirrorVertex mv4;
	mv4.pos = glm::vec3(m.pos2.x, m.pos1.y, m.pos2.z);
	mv4.uv = glm::vec2(0, 1);

	v.push_back(mv1);
	v.push_back(mv2);
	v.push_back(mv3);

	v.push_back(mv1);
	v.push_back(mv3);
	v.push_back(mv4);

	return v;
}

GLuint MakeShader(GLuint t, std::string path)
{
	//std::cout << path << std::endl;
	std::ifstream file(path.c_str(), std::ios::in);
	std::ostringstream contents;
	contents << file.rdbuf();
	file.close();

	const auto content = contents.str();
	//std::cout << content << std::endl;

	const auto s = glCreateShader(t);

	GLint sizes[] = { (GLint)content.size() };
	const auto data = content.data();

	glShaderSource(s, 1, &data, sizes);
	glCompileShader(s);

	GLint success;
	glGetShaderiv(s, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		GLchar infoLog[512];
		GLsizei l;
		glGetShaderInfoLog(s, 512, &l, infoLog);

		std::cout << infoLog << std::endl;
	}

	return s;
}

GLuint AttachAndLink(std::vector<GLuint> shaders)
{
	const auto prg = glCreateProgram();
	for (const auto s : shaders)
	{
		glAttachShader(prg, s);
	}

	glLinkProgram(prg);

	GLint success;
	glGetProgramiv(prg, GL_LINK_STATUS, &success);
	if (!success)
	{
		GLchar infoLog[512];
		GLsizei l;
		glGetProgramInfoLog(prg, 512, &l, infoLog);

		std::cout << infoLog << std::endl;
	}

	return prg;
}

int main(void)
{
	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
		std::exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

	window = glfwCreateWindow(1080, 720, "Majestic flying dragon", NULL, NULL);

	if (!window)
	{
		glfwTerminate();
		std::exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	// NOTE: OpenGL error checks have been omitted for brevity

	if (!gladLoadGL()) {
		std::cerr << "Something went wrong!" << std::endl;
		std::exit(-1);
	}

	// Callbacks
	glDebugMessageCallback(opengl_error_callback, nullptr);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

	const size_t nParticules = 200;
	auto particules = MakeParticules(nParticules);

	//STL
	const auto stl_triangles = ReadStl("dragon.stl");
	const size_t stl_nTriangles = stl_triangles.size();

	//PLY
	std::ifstream ss("dragon_ply/Dragon 2.5_ply.ply", std::ios::binary);
	tinyply::PlyFile file;
	file.parse_header(ss);
	auto ply_vertices = file.request_properties_from_element("vertex", { "x", "y", "z", "nx", "ny", "nz", "s", "t" });
	auto ply_faces = file.request_properties_from_element("face", { "vertex_indices" }, 3);
	file.read(ss);

	Image tex = LoadImage("dragon_ply/textures/Dragon_ground_color.bmp");

	// Shader
	const auto vertex_particules = MakeShader(GL_VERTEX_SHADER, "shader-particule.vert");
	const auto fragment_particules = MakeShader(GL_FRAGMENT_SHADER, "shader-particule.frag");

	const auto program_particules = AttachAndLink({ vertex_particules, fragment_particules });

	const auto vertex_stl = MakeShader(GL_VERTEX_SHADER, "shader-stl.vert");
	const auto geom_stl = MakeShader(GL_GEOMETRY_SHADER, "shader-stl.geom");
	const auto fragment_stl = MakeShader(GL_FRAGMENT_SHADER, "shader-stl.frag");

	const auto program_stl = AttachAndLink({ vertex_stl, geom_stl, fragment_stl });

	const auto vertex_ply = MakeShader(GL_VERTEX_SHADER, "shader-ply.vert");
	const auto fragment_ply = MakeShader(GL_FRAGMENT_SHADER, "shader-ply.frag");

	const auto program_ply = AttachAndLink({ vertex_ply, fragment_ply });

	const auto vertex_mirror = MakeShader(GL_VERTEX_SHADER, "shader-mirror.vert");
	const auto fragment_mirror = MakeShader(GL_FRAGMENT_SHADER, "shader-mirror.frag");

	const auto program_mirror = AttachAndLink({ vertex_mirror, fragment_mirror });

	glUseProgram(program_particules);

	// Buffers
	GLuint vbo_particule, vao_particule;
	glGenBuffers(1, &vbo_particule);
	glGenVertexArrays(1, &vao_particule);

	GLuint vbo_stl, vao_stl;
	glGenBuffers(1, &vbo_stl);
	glGenVertexArrays(1, &vao_stl);

	GLuint vbo_ply, vboe_ply, vao_ply, tex_ply;
	glGenBuffers(1, &vbo_ply);
	glGenBuffers(1, &vboe_ply);
	glGenVertexArrays(1, &vao_ply);
	glCreateTextures(GL_TEXTURE_2D, 1, &tex_ply);

	//PARTICULES
	glBindVertexArray(vao_particule);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_particule);
	glBufferData(GL_ARRAY_BUFFER, nParticules * sizeof(Particule), particules.data(), GL_STATIC_DRAW);

	// Bindings
	const auto index = glGetAttribLocation(program_particules, "position");
	glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, sizeof(Particule), nullptr);
	glEnableVertexAttribArray(index);

	const auto color = glGetAttribLocation(program_particules, "particuleColor");
	glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, sizeof(Particule), (const void *)sizeof(glm::vec3) );
	glEnableVertexAttribArray(color);

	//Point size
	glPointSize(5.f);

	//STL
	glBindVertexArray(vao_stl);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_stl);
	glBufferData(GL_ARRAY_BUFFER, stl_nTriangles * sizeof(Triangle), stl_triangles.data(), GL_STATIC_DRAW);

	// Bindings
	const auto position = glGetAttribLocation(program_stl, "position");
	glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), nullptr);
	glEnableVertexAttribArray(position);

	//PLY
	glBindVertexArray(vao_ply);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_ply);
	glBufferData(GL_ARRAY_BUFFER, ply_vertices->count * (8 * sizeof(float)), ply_vertices->buffer.get(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboe_ply);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ply_faces->count * (3 * sizeof(unsigned int)), ply_faces->buffer.get(), GL_STATIC_DRAW);
	// Bindings
	const auto ply_position = glGetAttribLocation(program_ply, "position");
	glVertexAttribPointer(ply_position, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
	glEnableVertexAttribArray(ply_position);

	const auto ply_normal = glGetAttribLocation(program_ply, "normal");
	glVertexAttribPointer(ply_normal, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (const void *) (3 * sizeof(float)));
	glEnableVertexAttribArray(ply_normal);

	const auto ply_uv = glGetAttribLocation(program_ply, "uv");
	glVertexAttribPointer(ply_uv, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (const void *) (6 * sizeof(float)));
	glEnableVertexAttribArray(ply_uv);

	// Texture
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTextureStorage2D(tex_ply, 1, GL_RGB8,tex.width, tex.height);
	glTextureSubImage2D(tex_ply, 0, 0, 0, tex.width, tex.height, GL_RGB, GL_UNSIGNED_BYTE, tex.data.data());
	glBindTextureUnit(0, tex_ply);
	glProgramUniform1i(program_ply, glGetUniformLocation(program_ply, "tex"), 0);

	glEnable(GL_DEPTH_TEST);

	//MATRIX DRAGON TRANSFORM
	glm::mat4 initPos = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.5f, 0.0f));
	glm::mat4 initRot = glm::rotate(glm::mat4(1.0f), glm::half_pi<float>(), glm::vec3(-1.0f, 0.0f, 0.0f));
	glm::mat4 initScale = glm::scale(glm::mat4(1.0f), glm::vec3(0.01f, 0.01f, 0.01f));
	glm::mat4 scenePos = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f));

	//MATRIX CAMERA TRANSFORM
	glm::vec3 camPos = glm::vec3(0.0f, 0.5f, -1.0f);
	glm::vec3 camForward = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 camUp = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::mat4 lookAtCam = glm::lookAt(camPos, camForward, camUp);

	//MATRIX TRANSFORM STEPS
	glm::mat4 transformLocal = initPos * initRot * initScale;
	glm::mat4 transformGlobal = scenePos;
	glm::mat4 transformCamera = lookAtCam;
	glm::mat4 transformProjection;

	//LIGHTS
	const int nLights = 2;
	glm::vec4 light_positions[nLights];
	light_positions[0] = glm::vec4(-50.0f, 0, -50.0f, 1.0f);
	light_positions[1] = glm::vec4(50.0f, 50.0f, 50.0f, 1.0f);

	//light_positions[0] = transformCamera * light_positions[0];
	//light_positions[1] = transformCamera * light_positions[1];

	glm::vec4 light_colors[nLights];
	light_colors[0] = glm::vec4(0.3f, 1.0f, 0.3f, 1.0f);
	light_colors[1] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

	float light_intensities[nLights];
	light_intensities[0] = 60000;
	light_intensities[1] = 80000;

	//MIRROR
	Mirror m;
	m.pos1 = glm::vec3(1.2f, 0.2f, 1.5f);
	m.pos2 = glm::vec3(0.4f, -0.6f, 1.8f);
	std::vector<MirrorVertex> m_vertex = MakeMirrorVertex(m);

	GLuint vao_mirror, vbo_mirror, mirror, tex_mirror;
	glGenBuffers(1, &vbo_mirror);
	glGenVertexArrays(1, &vao_mirror);
	glCreateFramebuffers(1, &mirror);
	glCreateTextures(GL_TEXTURE_2D, 1, &tex_mirror);
	glm::vec2 tex_mirror_size = glm::vec2(1080.0f, 720.0f);
	glTextureStorage2D(tex_mirror, 1, GL_RGB8, static_cast<unsigned int> (tex_mirror_size.x), static_cast<unsigned int> (tex_mirror_size.y));
	glNamedFramebufferTexture(mirror, GL_COLOR_ATTACHMENT0, tex_mirror, 0);

	if (glCheckNamedFramebufferStatus(mirror, GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cerr << "ERROR : framebuffer error\n";

	glBindVertexArray(vao_mirror);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_mirror);
	glBufferData(GL_ARRAY_BUFFER, m_vertex.size() * sizeof(MirrorVertex), m_vertex.data(), GL_STATIC_DRAW);
	const auto mirror_pos = glGetAttribLocation(program_mirror, "position");
	glVertexAttribPointer(mirror_pos, 3, GL_FLOAT, GL_FALSE, sizeof(MirrorVertex), nullptr);
	glEnableVertexAttribArray(mirror_pos);
	//const auto mirror_uv = glGetAttribLocation(program_mirror, "uv");
	//glVertexAttribPointer(mirror_uv, 2, GL_FLOAT, GL_FALSE, sizeof(MirrorVertex), (const void *) (3 * sizeof(float)));
	//glEnableVertexAttribArray(mirror_uv);


	glm::vec3 normal = glm::normalize(glm::cross(m_vertex[1].pos - m_vertex[0].pos, m_vertex[5].pos - m_vertex[0].pos));

	glm::mat4 mirrorSpace = glm::lookAt(m_vertex[0].pos, m_vertex[0].pos - normal, m_vertex[5].pos - m_vertex[0].pos);
	glm::mat4 mirrorScale = glm::scale(glm::mat4(1.0f), glm::vec3(1.0f, 1.0f, -1.0f));
	glm::mat4 mirrorTransform = glm::inverse(mirrorSpace) * mirrorScale * mirrorSpace;

	glm::vec3 camMirrorPos = mirrorTransform * glm::vec4(camPos, 1);
	glm::vec3 camMirrorForward = mirrorTransform * glm::vec4(camForward, 1);
	glm::vec3 camMirrorUp = mirrorTransform * glm::vec4(camUp, 1);

	glm::mat4 lookAtMirror = glm::lookAt(camMirrorPos, camMirrorForward, camMirrorUp);
	//glm::mat4 lookAtMirror = mirrorTransform;

	while (!glfwWindowShouldClose(window))
	{

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, mirror);
		glViewport(0, 0, static_cast<unsigned int> (tex_mirror_size.x), static_cast<unsigned int> (tex_mirror_size.y));
		glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		glViewport(0, 0, width, height);
			
		glClearColor(0, 0, 0, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		

		////DRAW STL
		//glUseProgram(program_stl);

		////transform
		//glm::mat4 transformWorld = transformLocal;
		//transformWorld = glm::rotate(glm::mat4(1.0), glm::half_pi<float>()  * (float)glfwGetTime(), glm::vec3(0.0f, 1.0f, 0.0f)) * transformWorld;
		//transformWorld = transformGlobal * transformWorld;
		//transformProjection = glm::perspective<float>(glm::radians(45.0f), (float)width / height, 0.001f, 20.0f);
		//glProgramUniformMatrix4fv(program_stl, glGetUniformLocation(program_stl, "transformWorld"), 1, GL_FALSE , (const float *) &transformWorld);
		//glProgramUniformMatrix4fv(program_stl, glGetUniformLocation(program_stl, "transformCamera"), 1, GL_FALSE , (const float *) &transformCamera);
		//glProgramUniformMatrix4fv(program_stl, glGetUniformLocation(program_stl, "transformProjection"), 1, GL_FALSE , (const float *) &transformProjection);

		////lights
		//glm::vec4 light_ambient = glm::vec4(0, 0, 0, 0);
		//float ambient_intesity = 0;
		//for (int i = 0; i < nLights; ++i)
		//{
		//	ambient_intesity += glm::length(light_intensities[i]);
		//	light_ambient += light_intensities[i] * light_colors[i];
		//}
		//light_ambient /= ambient_intesity;
		//
		//glProgramUniform1i(program_stl, glGetUniformLocation(program_stl, "light_number"), nLights);
		//glProgramUniform4fv(program_stl, glGetUniformLocation(program_stl, "light_positions"), nLights, (const float *)&light_positions);
		//glProgramUniform4fv(program_stl, glGetUniformLocation(program_stl, "light_colors"), nLights, (const float *)&light_colors);
		//glProgramUniform1fv(program_stl, glGetUniformLocation(program_stl, "light_intensities"), nLights, (const float *)&light_intensities);
		//glProgramUniform4fv(program_stl, glGetUniformLocation(program_stl, "light_ambient"), 1, (const float *)&light_ambient);
		//
		//glBindVertexArray(vao_stl);
		//glDrawArrays(GL_TRIANGLES, 0, 3 * stl_nTriangles);
		////END 

		//DRAW PLY
		glUseProgram(program_ply);

		//transform
		glm::mat4 transformWorld = transformLocal;
		transformWorld = glm::rotate(glm::mat4(1.0), glm::half_pi<float>()  * (float)glfwGetTime(), glm::vec3(0.0f, 1.0f, 0.0f)) * transformWorld;
		transformWorld = transformGlobal * transformWorld;
		transformProjection = glm::perspective<float>(glm::radians(45.0f), (float)width / height, 0.001f, 20.0f);
		
		glProgramUniformMatrix4fv(program_ply, glGetUniformLocation(program_ply, "transformWorld"), 1, GL_FALSE, (const float *)&transformWorld);
		glProgramUniformMatrix4fv(program_ply, glGetUniformLocation(program_ply, "transformProjection"), 1, GL_FALSE, (const float *)&transformProjection);

		//lights
		glm::vec4 light_ambient = glm::vec4(0, 0, 0, 0);
		float ambient_intesity = 0;
		for (int i = 0; i < nLights; ++i)
		{
			ambient_intesity += glm::length(light_intensities[i]);
			light_ambient += light_intensities[i] * light_colors[i];
		}
		light_ambient /= ambient_intesity;

		glProgramUniform1i(program_ply, glGetUniformLocation(program_ply, "light_number"), nLights);
		glProgramUniform4fv(program_ply, glGetUniformLocation(program_ply, "light_positions"), nLights, (const float *)&light_positions);
		glProgramUniform4fv(program_ply, glGetUniformLocation(program_ply, "light_colors"), nLights, (const float *)&light_colors);
		glProgramUniform1fv(program_ply, glGetUniformLocation(program_ply, "light_intensities"), nLights, (const float *)&light_intensities);
		glProgramUniform4fv(program_ply, glGetUniformLocation(program_ply, "light_ambient"), 1, (const float *)&light_ambient);


		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, mirror);
		glViewport(0, 0, static_cast<unsigned int> (tex_mirror_size.x), static_cast<unsigned int> (tex_mirror_size.y));

		transformCamera = lookAtMirror;
		glProgramUniformMatrix4fv(program_ply, glGetUniformLocation(program_ply, "transformCamera"), 1, GL_FALSE, (const float *)&transformCamera);

		glBindTextureUnit(0, tex_ply);
		glBindVertexArray(vao_ply);
		glDrawElements(GL_TRIANGLES, ply_faces->count * 3, GL_UNSIGNED_INT, 0);

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glViewport(0, 0, width, height);

		transformCamera = lookAtCam;
		glProgramUniformMatrix4fv(program_ply, glGetUniformLocation(program_ply, "transformCamera"), 1, GL_FALSE, (const float *)&transformCamera);

		glBindVertexArray(vao_ply);
		glDrawElements(GL_TRIANGLES, ply_faces->count * 3, GL_UNSIGNED_INT, 0);
		//END 

		//DRAW MIRROR
		glUseProgram(program_mirror);

		glBindTextureUnit(1, tex_mirror);
		glProgramUniform1i(program_mirror, glGetUniformLocation(program_mirror, "tex"), 1);
		transformCamera = lookAtCam;
		glProgramUniform2fv(program_mirror, glGetUniformLocation(program_mirror, "cameraMirrorSize"), 1, &tex_mirror_size[0]);
		glProgramUniformMatrix4fv(program_mirror, glGetUniformLocation(program_mirror, "transformCamera"), 1, GL_FALSE, (const float *)&transformCamera);
		glProgramUniformMatrix4fv(program_mirror, glGetUniformLocation(program_mirror, "transformProjection"), 1, GL_FALSE, (const float *)&transformProjection);

		glBindVertexArray(vao_mirror);
		glDrawArrays(GL_TRIANGLES, 0, m_vertex.size());
		//END

		//DRAW PARITCULES
		glUseProgram(program_particules);
		glBindVertexArray(vao_particule);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_particule);
		MoveParticules(particules);
		glBufferSubData(GL_ARRAY_BUFFER, 0, nParticules * sizeof(Particule), particules.data());
		glDrawArrays(GL_POINTS, 0, nParticules);
		//END 

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glfwDestroyWindow(window);
	glfwTerminate();
	std::exit(EXIT_SUCCESS);
}
