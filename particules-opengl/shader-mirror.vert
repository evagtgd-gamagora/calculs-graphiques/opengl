#version 450

uniform mat4 transformCamera;
uniform mat4 transformProjection;

in vec3 position;
in vec2 uv;


out vec4 positionCameraSpace;
out vec4 cColor;
out vec2 uUv;

void main()
{
	positionCameraSpace = transformCamera * vec4(position, 1.0f);

    gl_Position = transformProjection * positionCameraSpace;
    //gl_Position = vec4(position, 1.0f);

	cColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	//nNormal = normalize(transpose(inverse(transformCamera))  * vec4(normal, 0.0f));
	uUv = uv;
}