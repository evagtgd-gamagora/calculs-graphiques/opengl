#version 450

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec4[] color;
in vec4[] positionCameraSpace;

out vec3 normal;
out vec4 cColor;
out vec4 positionCS;

void main()
{
	vec4 p0 = gl_in[0].gl_Position;
	vec4 p1 = gl_in[1].gl_Position;
	vec4 p2 = gl_in[2].gl_Position;
	
	normal = normalize(cross(vec3(p1 - p0), vec3(p2 - p1)));
	
	gl_Position = p0;
	cColor = color[0];
	positionCS = positionCameraSpace[0];
	EmitVertex();
	
	gl_Position = p1;
	cColor = color[1];
	positionCS = positionCameraSpace[1];
	EmitVertex();
	
	gl_Position = p2;
	cColor = color[2];
	positionCS = positionCameraSpace[2];
	EmitVertex();
	
	EndPrimitive();
}

