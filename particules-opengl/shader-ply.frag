#version 450

uniform mat4 transformCamera;

uniform int light_number;
uniform vec4[10] light_positions;
uniform vec4[10] light_colors;
uniform float[10] light_intensities;
uniform vec4 light_ambient;

uniform sampler2D tex;

in vec4 positionCameraSpace;
in vec4 nNormal;
in vec2 uUv;

in vec4 cColor;

out vec4 color;

void main()
{

	vec4 lightColor = vec4(0,0,0,0);

	for(int i = 0; i < light_number; ++i)
	{
		vec4 pos = transformCamera * light_positions[i];
		float distance = length(pos - positionCameraSpace);
		vec4 light = normalize(pos - positionCameraSpace);
		vec4 cam = normalize(-positionCameraSpace);
		float cos_theta = max(dot(nNormal, light), 0);
		float cos_theta_p = max(dot(reflect(-light, nNormal), cam), 0);

		lightColor += light_colors[i] * light_intensities[i] *(0.625f * cos_theta + 0.375f * +  pow(cos_theta_p, 100)) / pow(distance, 2);
		//lightColor += light_colors[i] * (0.625f * cos_theta + 0.375f * +  pow(cos_theta_p, 100));
	}

	//color = cColor * (0.2f * light_ambient + 0.8f * lightColor);									//light without texture
	//color = vec4(uUv, 0, 1);																		//UV
	//color = vec4(nNormal.xyz, 1.0f );																//normal
	//color = texture(tex, vec2(uUv.x, uUv.y));														//texture only
	color = texture(tex, vec2(uUv.x, uUv.y)) * (0.2f * light_ambient + 0.8f * lightColor);		//texture with light

}

