#version 450

uniform sampler2D tex;
uniform vec2 cameraMirrorSize;


in vec4 positionCameraSpace;
in vec2 uUv;

in vec4 cColor;

in vec4 gl_FragCoord;
out vec4 color;

void main()
{
	//color = cColor;		//texture with light
	//color = vec4(uUv, 0 , 1);
	//color = texture(tex, vec2(uUv.x, uUv.y));														//texture only

	//color = vec4( gl_FragCoord.x / cameraMirrorSize.x, gl_FragCoord.y/ cameraMirrorSize.y, 0 , 1);
	color = texture(tex, vec2(gl_FragCoord.y / cameraMirrorSize.y, gl_FragCoord.x/cameraMirrorSize.x));
}

