#version 450

uniform mat4 transformWorld;
uniform mat4 transformCamera;
uniform mat4 transformProjection;

in vec3 position;
out vec4 positionCameraSpace;
out vec4 color;

void main()
{
	vec4 positionCameraSpace = transformCamera * transformWorld * vec4(position, 1.0f);

    gl_Position = transformProjection * positionCameraSpace;

	color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
}