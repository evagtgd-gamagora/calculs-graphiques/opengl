#version 450

uniform mat4 transformWorld;
uniform mat4 transformCamera;
uniform mat4 transformProjection;

in vec3 position;
in vec3 normal;
in vec2 uv;

out vec4 positionCameraSpace;
out vec4 nNormal;
out vec2 uUv;

out vec4 cColor;

void main()
{
	positionCameraSpace = transformCamera * transformWorld * vec4(position, 1.0f);

    gl_Position = transformProjection * positionCameraSpace;

	cColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	nNormal = normalize(transpose(inverse(transformCamera * transformWorld))  * vec4(normal, 0.0f));
	uUv = uv;
}