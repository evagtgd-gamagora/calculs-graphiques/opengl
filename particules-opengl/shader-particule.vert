#version 450

in vec3 position;
in vec3 particuleColor;
out vec4 colorPosition;
out vec4 particuleColor4;


vec4 colorFromPosition(vec3 position)
{
	return vec4(position, 1.0);
}

vec4 blueGradFromPosition(vec3 position)
{
	float factor = (position.y + 1) / 2.0;
	return vec4(factor, factor, 1.0, 1.0);
}


void main()
{
    gl_Position = vec4(position, 1.0);
	particuleColor4 =vec4(particuleColor, 1.0) ;
	colorPosition = blueGradFromPosition(position);
}